# _Animal Shelter_

#### _This is a site for animal adoption_

#### By _**Aimen K. Martin Cartledge**_

## Description
_This is a site for animal adoptions. Enjoy!_

## Setup/Installation Requirements
* _Clone_

## Known Bugs
_Not compatible with Windows7_

## Support and contact details
_email for additional setup instructions through github_

## Technologies Used
_HTML, css, Javascript, Jquery_

### License
* Epicodus

Copyright (c) 2016 **_Aimen K & Martin C._**
